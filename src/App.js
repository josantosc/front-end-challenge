import React, { useState } from "react";
import { fetch } from "./services/sessions";

function App() {
  const [data, setData] = useState([]);

  return (
    <div className="App">
      <button
        onClick={() => {
          const fetchedData = fetch;

          setData(fetchedData);
        }}
      >
        Pegar dados
      </button>
      {data.map((elem) => {
        return <div>{elem}</div>;
      })}
    </div>
  );
}

export default App;
